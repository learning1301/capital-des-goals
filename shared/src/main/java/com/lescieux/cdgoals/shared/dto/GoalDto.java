package com.lescieux.cdgoals.shared.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class GoalDto {

  private Long id;

  @NotBlank(message = "Name is mandatory")
  @Size(max = 100, message = "Name's maximum length is 100 characters")
  private String name;

  @Size(max = 2000, message = "Description's maximum length is 2000 characters")
  private String description;
}
