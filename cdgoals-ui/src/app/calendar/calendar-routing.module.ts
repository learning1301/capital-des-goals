import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { CalendarComponent } from './calendar.component';
import { CalendarHomeComponent } from "./home/calendar-home.component";

const routes: Routes = [
  {
    path: 'calendar',
    component: CalendarComponent,
    data: { breadcrumb: { alias: 'Calendar' } },
    children: [
      {
        path: '',
        component: CalendarHomeComponent,
        data: { breadcrumb: { alias: 'Home' } }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule { }