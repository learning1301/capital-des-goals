// Framework modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Custom modules
import { SharedModule } from '../shared/shared.module';
import { CalendarRoutingModule } from './calendar-routing.module';

// Components
import { CalendarHomeComponent } from './home/calendar-home.component';
import { CalendarComponent } from './calendar.component';



@NgModule({
  declarations: [
    CalendarHomeComponent,
    CalendarComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CalendarRoutingModule
  ]
})
export class CalendarModule { }
