import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  breadcrumb: { alias: string, url: string }[] = [];
  currentStep = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.breadcrumb = this.route.pathFromRoot.map(ar => ar.snapshot)
    // Get all the steps except the last one
    .filter(ars => ars?.data['breadcrumb'] != null)
    .filter(ars => ars.component !== this.route.snapshot.component)
    .map((ars, i, arr) => {
      return {
        alias: ars.data['breadcrumb'].alias,
        url: '../'.repeat(arr.length - i)
      };
    });

    this.currentStep = this.route.snapshot.data['breadcrumb']?.alias;
  }
}
