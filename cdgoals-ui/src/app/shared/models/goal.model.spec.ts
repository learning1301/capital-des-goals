import { Goal } from './goal.model';

describe('Goal', () => {
  it('should create an instance', () => {
    expect(<Goal>{id: 1, name: 'test goal', description: 'have all tests green'}).toBeTruthy();
  });
});
