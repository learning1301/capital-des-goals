// Framework modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
// Material design
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
// Custom modules
import { GoalsHomeComponent } from './home/goal-home.component';
import { GoalsRoutingModule } from './goals-routing.module';
import { SharedModule } from '../shared/shared.module';
// Other components
import { GoalService } from './services/goal.service';
import { GoalCreateComponent } from './create/goal-create.component';
import { GoalsComponent } from './goals.component';



@NgModule({
  declarations: [
    GoalsHomeComponent,
    GoalCreateComponent,
    GoalsComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    SharedModule,
    GoalsRoutingModule
  ],
  providers: [
    GoalService
  ]
})
export class GoalsModule { }
