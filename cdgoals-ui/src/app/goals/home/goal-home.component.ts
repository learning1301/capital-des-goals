import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Goal } from 'src/app/shared/models/goal.model';
import { GoalService } from '../services/goal.service';

@Component({
  selector: 'app-home',
  templateUrl: './goal-home.component.html',
  styleUrls: ['./goal-home.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ])
  ]
})
export class GoalsHomeComponent implements OnInit {

  goals: Goal[] = [];
  displayedColumns: string[] = ['id', 'name'];
  displayedColumnsWithExpand = [...this.displayedColumns, 'expand'];
  expandedGoal: Goal | null = null;

  constructor(private goalService: GoalService) { }

  ngOnInit() {
    this.goalService.getGoals().subscribe(g => this.goals = g);
  }
}
