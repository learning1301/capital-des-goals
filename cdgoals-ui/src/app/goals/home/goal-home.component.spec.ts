import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { GoalsModule } from '../goals.module';
import { GoalService } from '../services/goal.service';

import { GoalsHomeComponent } from './goal-home.component';

describe('HomeComponent', () => {
  let component: GoalsHomeComponent;
  let fixture: ComponentFixture<GoalsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GoalsHomeComponent],
      imports: [RouterTestingModule, GoalsModule],
      providers: [GoalService]
    }).compileComponents();

    fixture = TestBed.createComponent(GoalsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
