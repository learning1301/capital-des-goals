import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GoalCreateComponent } from './create/goal-create.component';
import { GoalsComponent } from './goals.component';
import { GoalsHomeComponent } from './home/goal-home.component';

const routes: Routes = [
  {
    path: 'goals',
    component: GoalsComponent,
    data: { breadcrumb: { alias: 'Goals' } },
    children: [
      {
        path: '',
        component: GoalsHomeComponent,
        data: { breadcrumb: { alias: 'Home' } }
      },
      {
        path: 'create',
        component: GoalCreateComponent,
        data: { breadcrumb: { alias: 'Creation' } }
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoalsRoutingModule { }
