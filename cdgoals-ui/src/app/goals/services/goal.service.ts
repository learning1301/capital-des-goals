import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Goal } from 'src/app/shared/models/goal.model';

const GOALS_QUERY_URL = 'http://goals-query.127.0.0.1.nip.io/goals';
const GOALS_COMMAND_URL = 'http://goals-command.127.0.0.1.nip.io/goals';

@Injectable()
export class GoalService {

  constructor(private http: HttpClient) { }

  public getGoals(): Observable<Goal[]> {
    return this.http.get<Goal[]>(GOALS_QUERY_URL);
  }

  public createGoal(goal: Goal): Observable<any> {
    return this.http.post(GOALS_COMMAND_URL, goal);
  }
}
