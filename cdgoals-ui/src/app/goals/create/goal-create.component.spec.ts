import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { GoalsRoutingModule } from '../goals-routing.module';
import { GoalsModule } from '../goals.module';
import { GoalService } from '../services/goal.service';

import { GoalCreateComponent } from './goal-create.component';

describe('GoalsCreateComponent', () => {
  let component: GoalCreateComponent;
  let fixture: ComponentFixture<GoalCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GoalCreateComponent],
      imports: [RouterTestingModule, GoalsModule, GoalsRoutingModule],
      providers: [GoalService]
    })
      .compileComponents();

    fixture = TestBed.createComponent(GoalCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
