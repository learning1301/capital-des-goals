import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Goal } from 'src/app/shared/models/goal.model';
import { GoalService } from '../services/goal.service';

@Component({
  selector: 'app-goals-create',
  templateUrl: './goal-create.component.html',
  styleUrls: ['./goal-create.component.scss']
})
export class GoalCreateComponent implements OnInit {

  goalForm: FormGroup;
  nameFormControl: FormControl<string | null>;
  descriptionFormControl: FormControl<string | null>;


  constructor(
    private goalService: GoalService,
    private router: Router
  ) {
    this.goalForm = new FormGroup({
      name: new FormControl({ value: null, disabled: false }, [Validators.required, Validators.maxLength(100)]),
      description: new FormControl({ value: null, disabled: false }, [Validators.maxLength(2000)])
    });
    this.nameFormControl = <FormControl<string | null>>this.goalForm.get('name');
    this.descriptionFormControl = <FormControl<string | null>>this.goalForm.get('description');
  }

  ngOnInit(): void {
    this.nameFormControl.valueChanges.subscribe(v => console.log(this.nameFormControl.errors));
  }

  createGoal(): void {
    let goal: Goal = this.goalForm.value;
    this.goalService.createGoal(goal).subscribe(
      r => this.router.navigate(['../'])
    );
  }
}
