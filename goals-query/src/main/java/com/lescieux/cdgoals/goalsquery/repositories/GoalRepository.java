package com.lescieux.cdgoals.goalsquery.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lescieux.cdgoals.goalsquery.entities.Goal;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {

}
