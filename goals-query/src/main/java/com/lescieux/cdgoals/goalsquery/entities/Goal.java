package com.lescieux.cdgoals.goalsquery.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "GOAL")
@Entity
public class Goal {

  @Id
  @GeneratedValue(generator = "SEQ_GOAL", strategy = GenerationType.SEQUENCE)
  @SequenceGenerator(name = "SEQ_GOAL", sequenceName = "SEQ_GOAL", allocationSize = 1)
  private Long id;
  
  @Column(name = "NAME", length = 100)
  private String name;

  @Column(name = "DESCRIPTION", length = 2000)
  private String description;
}
