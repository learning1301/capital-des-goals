package com.lescieux.cdgoals.goalsquery.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lescieux.cdgoals.goalsquery.mappers.GoalMapper;
import com.lescieux.cdgoals.goalsquery.services.GoalService;
import com.lescieux.cdgoals.shared.dto.GoalDto;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("/goals")
public class GoalController {

  private final GoalMapper goalMapper;
  private final GoalService goalService;

  @GetMapping
  public Flux<GoalDto> getAllGoals() {
    return goalService.getAllGoals().map(goalMapper::toDto);
  }
}
