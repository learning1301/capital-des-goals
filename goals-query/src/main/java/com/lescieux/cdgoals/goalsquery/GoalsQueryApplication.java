package com.lescieux.cdgoals.goalsquery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoalsQueryApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoalsQueryApplication.class, args);
	}

}
