package com.lescieux.cdgoals.goalsquery.mappers;

import org.mapstruct.Mapper;

import com.lescieux.cdgoals.goalsquery.entities.Goal;
import com.lescieux.cdgoals.shared.dto.GoalDto;

@Mapper(componentModel = "spring")
public interface GoalMapper {

  GoalDto toDto(final Goal entity);
}
