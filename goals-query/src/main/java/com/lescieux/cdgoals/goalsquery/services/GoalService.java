package com.lescieux.cdgoals.goalsquery.services;

import org.springframework.stereotype.Service;

import com.lescieux.cdgoals.goalsquery.entities.Goal;
import com.lescieux.cdgoals.goalsquery.repositories.GoalRepository;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@Service
public class GoalService {

  private final GoalRepository goalRepository;

  public Flux<Goal> getAllGoals() {
    return Flux.fromIterable(goalRepository.findAll());
  }
}
