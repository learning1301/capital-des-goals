# Capitale des Goals


## Description
Capitale des Goals est un clin d'oeil à la ville de Lyon et un projet d'apprentissage visant à monter en compétences sur des technologies et notions techniques. Fonctionnellement, c'est un outil de gestion et de partage d'objectifs.

Les points suivants ont été identifiés pour la monté en compétences :
- Architecture microservices de type CQRS
- Pipeline d'intégration et livraison continue
- Infrastructure as Code
- Docker / Kubernetes / Helm
- Auto scalabilité
- Déploiement sans interruption de service
- Tests de performance
- Green IT
- Java 17 / SpringBoot 3 / GraalVM
- Optimisation de performance SQL
- Elastic Search
- MongoDB
- Bases de données distribuées

## Installation
Ce projet est en microservices et peut être installé en local via Maven et docker-compose ou sur un cluster Kubernetes.

### Installation locale pour un environnement de développement
Chaque microservice peut être exécuté via la commande suivante dans son propre dossier.
```
mvn spring-boot:run
```
Les microservices ont des dépendances sur des outils externes qui peuvent être exécutés via la commande docker-compose up à la racine.
Une fois que les changements réalisés dans le code sont satisfaisant, une image du microservice peut être générée via la commande suivante :
```
mvn spring-boot:build-image
```
Ou avec la commande suivante pour générer une image native avec GraalVM :
```
mvn spring-boot:build-image -Pnative
```

### Installation sur un cluster Kubernetes pour un environnement d'intégration
Les images des microservices doivent d'abord être générées (voir installation locale).
L'ensemble des microservices et des outils externes peuvent ensuite être installés via les commandes suivantes :
```
kubectl apply -f ingress-controller.yaml
kubectl apply -f cluster.yaml
```
Remarque : La configuration de l'ingress controller est différente en fonction de l'environnement Kubernetes, ici elle est adaptée à Docker Desktop.
Ils peuvent être désinstallés via :
```
kubectl delete -f ingress-controller.yaml
kubectl delete -f cluster.yaml
```
L'image du front front Angular se build pour l'instant via la commande (en remplaçant <version>) :
```
docker build -t cdgoals-ui:<version> .
```
Prochainement, une commande Maven permettra de builder toutes les images.

Une fois installée sur votre cluster Docker Desktop, l'application est accessible via http://capitale-des-goals.127.0.0.1.nip.io.

## Support
Aucun engagement n'est pris en terme de support mais nous pouvons éventuellement répondre à des tickets.

## Roadmap (prochains goals)
- Builder l'image cdgoals-ui via Maven
- Améliorer le rendu visuel de goals-ui, notamment avec Angular Material
- Persistence des commandes, commandes inverses et rejeu
- Variabiliser les hostnames hard codés, commencer à utiliser Helm
- Configurer l'intégration et le déploiement continu

## Auteurs
Thomas Lescieux, Matthieu Lescieux

## Licence
Licence BSD
