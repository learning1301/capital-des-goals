package com.lescieux.cdgoals.goalsstate.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.lescieux.cdgoals.shared.dto.GoalDto;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

  @Value(value = "${spring.kafka.bootstrap-servers}")
  private String bootstrapAddress = null;

  @Bean("goalConsumerFactory")
  public ConsumerFactory<String, GoalDto> goalConsumerFactory() {
    final Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "com.lescieux.cdgoals");
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    ErrorHandlingDeserializer<GoalDto> goalDeserializer = new ErrorHandlingDeserializer<>(new JsonDeserializer<>(GoalDto.class));
    return new DefaultKafkaConsumerFactory<>(props, new StringDeserializer(), goalDeserializer);
  }

  @Bean("goalListenerContainerFactory")
  public ConcurrentKafkaListenerContainerFactory<String, GoalDto> goalListenerContainerFactory() {
    final ConcurrentKafkaListenerContainerFactory<String, GoalDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(goalConsumerFactory());
    return factory;
  }

}
