package com.lescieux.cdgoals.goalsstate.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lescieux.cdgoals.goalsstate.entities.Goal;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {

}
