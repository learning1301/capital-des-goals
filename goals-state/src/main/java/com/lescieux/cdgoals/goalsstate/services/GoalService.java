package com.lescieux.cdgoals.goalsstate.services;

import org.springframework.stereotype.Service;

import com.lescieux.cdgoals.goalsstate.entities.Goal;
import com.lescieux.cdgoals.goalsstate.repositories.GoalRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class GoalService {

  private final GoalRepository goalRepository;

  public Long createGoal(Goal goal) {
    return goalRepository.save(goal).getId();
  }
}
