package com.lescieux.cdgoals.goalsstate.consumers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.lescieux.cdgoals.goalsstate.entities.Goal;
import com.lescieux.cdgoals.goalsstate.mappers.GoalMapper;
import com.lescieux.cdgoals.goalsstate.services.GoalService;
import com.lescieux.cdgoals.shared.dto.GoalDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class GoalConsumer {

  @Autowired
  private GoalMapper goalMapper;
  @Autowired
  private GoalService goalService;

  @Transactional
  @KafkaListener(topics = "createGoal", containerFactory = "goalListenerContainerFactory")
  public void createGoal(GoalDto dto) {
    log.info("receive payload");
    final Goal goal = goalMapper.toEntity(dto);
    Long goalId = goalService.createGoal(goal);
    log.info("Goal with id {} saved in database", goalId);
  }
}
