package com.lescieux.cdgoals.goalsstate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoalsStateApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoalsStateApplication.class, args);
	}

}
