package com.lescieux.cdgoals.goalsstate.mappers;

import org.mapstruct.Mapper;

import com.lescieux.cdgoals.goalsstate.entities.Goal;
import com.lescieux.cdgoals.shared.dto.GoalDto;

@Mapper(componentModel = "spring")
public interface GoalMapper {

  Goal toEntity(final GoalDto dto);

  GoalDto toDto(final Goal entity);

}
