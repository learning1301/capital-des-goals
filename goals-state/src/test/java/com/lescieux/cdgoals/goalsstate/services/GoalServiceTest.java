package com.lescieux.cdgoals.goalsstate.services;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.lescieux.cdgoals.goalsstate.entities.Goal;
import com.lescieux.cdgoals.goalsstate.repositories.GoalRepository;

@ExtendWith(MockitoExtension.class)
class GoalServiceTest {

  private static final String GOAL_NAME = "Sample goal";
  private static final String GOAL_DESCRIPTION = "This is an example of goal for integration testing";

  private GoalService goalService;

  @Mock
  private GoalRepository goalRepository;

  @BeforeEach
  public void setUp() {
    goalService = new GoalService(goalRepository);
  }

  @Test
  void createGoalN01() {
    // Given
    final Goal input = Goal.builder().name(GOAL_NAME).description(GOAL_DESCRIPTION).build();

    // When
    Mockito.when(goalRepository.save(Mockito.any())).thenAnswer(inv -> {
      final Goal param = inv.getArgument(0, Goal.class);
      param.setId(1L);
      return param;
    });
    final Long result = goalService.createGoal(input);

    // Then
    Assertions.assertNotNull(result);
    Mockito.when(goalRepository.findById(result)).thenReturn(Optional.of(input));
    final Optional<Goal> requestedOpt = goalRepository.findById(result);
    Assertions.assertTrue(requestedOpt.isPresent());
    final Goal requested = requestedOpt.get();
    Assertions.assertEquals(GOAL_NAME, requested.getName(), "The goal name found in database must be the same than the saved one");
    Assertions.assertEquals(GOAL_DESCRIPTION, requested.getDescription(),
        "The goal description found in database must be the same than the saved one");
  }
}
