package com.lescieux.cdgoals.goalsstate.services;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.lescieux.cdgoals.goalsstate.config.ContextTestConfig;
import com.lescieux.cdgoals.goalsstate.entities.Goal;
import com.lescieux.cdgoals.goalsstate.repositories.GoalRepository;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ContextTestConfig.class)
class GoalServiceIT {

  private static final String GOAL_NAME = "Sample goal";
  private static final String GOAL_DESCRIPTION = "This is an example of goal for integration testing";

  @Autowired
  private GoalService goalService;

  @Autowired
  private GoalRepository goalRepository;

  @Test
  void createGoalN01() {
    // Given
    final Goal input = Goal.builder().name(GOAL_NAME).description(GOAL_DESCRIPTION).build();

    // When
    final Long result = goalService.createGoal(input);

    // Then
    Assertions.assertNotNull(result);
    final Optional<Goal> requestedOpt = goalRepository.findById(result);
    Assertions.assertTrue(requestedOpt.isPresent());
    final Goal requested = requestedOpt.get();
    Assertions.assertEquals(GOAL_NAME, requested.getName(), "The goal name found in database must be the same than the saved one");
    Assertions.assertEquals(GOAL_DESCRIPTION, requested.getDescription(),
        "The goal description found in database must be the same than the saved one");
  }
}
