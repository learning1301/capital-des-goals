package com.lescieux.cdgoals.goalsstate.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;

import com.lescieux.cdgoals.goalsstate.GoalsStateApplication;

@ActiveProfiles("test")
@Configuration
@ComponentScan(basePackageClasses = GoalsStateApplication.class)
@PropertySource("classpath:application.properties")
public class ContextTestConfig {

}
