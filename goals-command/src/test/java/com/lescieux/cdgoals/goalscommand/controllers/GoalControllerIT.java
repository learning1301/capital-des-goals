package com.lescieux.cdgoals.goalscommand.controllers;

import static org.mockito.Mockito.times;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.lescieux.cdgoals.goalscommand.config.SecurityWebTestConfig;
import com.lescieux.cdgoals.goalscommand.producers.GoalProducer;
import com.lescieux.cdgoals.shared.dto.GoalDto;

import reactor.core.publisher.Mono;

@WebFluxTest
@Import(SecurityWebTestConfig.class)
class GoalControllerIT {

  @MockBean
  private GoalProducer goalProducer;

  @Autowired
  private WebTestClient testClient;

  @Test
  void postGoalN01() throws Exception {
    // Given
    final GoalDto goal = GoalDto.builder().name("Sample goal").description("This is an example of goal for integration testing").build();

    // When
    testClient.post().uri("/goals").body(Mono.just(goal), GoalDto.class).exchange().expectStatus().isOk();

    // Then
    Mockito.verify(goalProducer, times(1)).createGoal(goal);
  }
}
