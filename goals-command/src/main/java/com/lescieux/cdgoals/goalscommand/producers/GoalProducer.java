package com.lescieux.cdgoals.goalscommand.producers;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.lescieux.cdgoals.shared.dto.GoalDto;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class GoalProducer {

  private static final String TOPIC_CREATE_GOAL = "createGoal";

  private final KafkaTemplate<String, GoalDto> goalKafkaTemplate;

  public void createGoal(final GoalDto goal) {
    goalKafkaTemplate.send(TOPIC_CREATE_GOAL, goal);
  }
}
