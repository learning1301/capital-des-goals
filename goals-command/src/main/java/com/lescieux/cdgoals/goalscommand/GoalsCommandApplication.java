package com.lescieux.cdgoals.goalscommand;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoalsCommandApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoalsCommandApplication.class, args);
	}

}
