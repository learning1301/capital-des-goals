package com.lescieux.cdgoals.goalscommand.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lescieux.cdgoals.goalscommand.producers.GoalProducer;
import com.lescieux.cdgoals.shared.dto.GoalDto;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@CrossOrigin
@RestController
@RequestMapping("/goals")
public class GoalController {

  private final GoalProducer goalProducer;

  @PostMapping
  public void postGoal(@RequestBody @Valid GoalDto goal) {
    goalProducer.createGoal(goal);
  }
}
